import React,{useEffect} from 'react'
import { View, Text } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import Navigation from './src/navigation/Navigation'
import Demo from './Demo'




export default function App() {

  useEffect(() => {
    // Update the document title using the browser API
    setTimeout(()=>{ 
      SplashScreen.hide();

      }, 500);
  });
  return (
    <View style={{flex:1}}>
    {/* <Navigation/>
     */}
     <Demo/>
   </View>
  )
}
