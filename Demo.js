
import React, {useState,useRef} from 'react';
import {
  Button,

  StyleSheet,
  View,
} from 'react-native';
import { Card, Title, Paragraph } from 'react-native-paper';


import Share from 'react-native-share';
import ViewShot,{captureRef}  from 'react-native-view-shot';


// import images from './src/images/imagebase64';
// import pdfBase64 from './src/images/pdfbase64';

const Demo = () => {
  const [snapshot, setSnapshot] = useState('');

 

  const card = useRef(null);

 

 
 

  const takeSnapshot = async () => {
      const snapshot = await captureRef(card, {
        result: 'data-uri',
      });

      console.log(snapshot)
     await setSnapshot(snapshot)

     shareSnapshot()
      
  };
   const shareSnapshot = () => {
    if (snapshot) {
      Share.open({
        url: snapshot,
      })
        .then((res: any) => {
          console.log(res);
        })
        .catch((err: any) => {
          err && console.log(err);
        });
    }
  };

 
  





  return (
    <View style={styles.container}>
              <ViewShot ref={card}>

       <Card style={styles.card}>
        
          <Card.Content>
            <Title>hello card</Title>
            <Paragraph>content</Paragraph>
          </Card.Content>
          {/* <Card.Cover source={{uri: 'https://picsum.photos/700'}} /> */}
           
        </Card>

      
        </ViewShot>
        <Button
            title='share '
            onPress={takeSnapshot}></Button>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
   width: 300,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

});

export default Demo;
