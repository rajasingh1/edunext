import React from 'react'
import { View, Text,StyleSheet,Image,Pressable,ImageBackground } from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';

export default function Index() {
    return (
        <View>
           <View style={styles.header}>

          <Pressable>
          <Image
            style={styles.image}
            source={require('../../assests/menu.png')}
          />
          </Pressable>

          <Text style={styles.text}>Edunext Public School</Text>
           </View>
           <View>

           </View>
           <View>
           <ImageBackground
          source={require('../../assests/shape.png')}
          style={styles.backgroundImage}>
<View style={styles.display}>
   <View >
   <Image
            style={styles.profile}
            source={{
          uri:
            'https://img.etimg.com/thumb/msid-75201770,width-650,imgsize-212710,,resizemode-4,quality-100/adventures-of-chhota-bheem-and-his-friends-will-be-aired-on-dd-national-every-afternoon-at-2-pm-.jpg',
        }}
          />
   </View>
   <View>
       <Text>Yukund Bansal</Text>
       <Text>Admission No :DB171515</Text>

   </View>
</View>
        </ImageBackground>


           </View>
        </View>
    )
}
let styles = StyleSheet.create({
    header: {
  backgroundColor:'skyblue',
      width: wp('100'),
      height: hp('9'),
      flexDirection:'row',
      alignItems: 'center'
   
    },
    image:{
        width: wp('10'),
        height: hp('3')
    },
    text:{
        fontSize: hp('2.8'),
        fontWeight:'bold',
        color:'#fff',
        marginLeft:wp('8')
    },
    backgroundImage: {
        resizeMode: 'cover',
        width: wp('100'),
        height: hp('30'),
        alignItems: 'center',
        // or 'stretch'
      },
      profile:{
        width: wp('28'),
        height: wp('28'),
        borderRadius: wp('28')/2


      },
      display: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:hp('3')
      }
     
    
  });