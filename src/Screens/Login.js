import React, {useState} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  Pressable,
  Text,
  ImageBackground
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function Login() {
  const [text, setText] = useState('');

  return (
    <View style={styles.container}>
          <ImageBackground
          source={require('../assests/login_background.png')}
          style={styles.backgroundImage}>
      <View>
        <Image source={require('../assests/login_logo.png')} />
      </View>
      <View>
        <TextInput
          placeholder={'Username'}
          style={styles.TextInput}
          onChangeText={(text) => setText(text)}
          value={text}
        />
      </View>

      <View>
        <TextInput
          secureTextEntry
          placeholder={'Password'}
          style={styles.TextInput}
          onChangeText={(text) => setText(text)}
          value={text}
        />
      </View>
      <Pressable style={styles.SignIn}>
        <Text style={{fontSize:19,color: '#fff'}}>Sign In</Text>
      </Pressable>
      <Pressable>
        <Text >Forgot Password</Text>
      </Pressable>
      <Pressable>
        <Text>Change Your school Codeß</Text>
      </Pressable>
      </ImageBackground>

    </View>
  );
}

let styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextInput: {
    width: wp('88'),

    height: hp('8'),
    borderWidth: 1,
    borderBottomColor: '#000',
    borderColor: 'transparent',
    fontSize: 20,
  },
  backgroundImage: {
    resizeMode: 'cover',
    width: wp('100'),
    height: hp('100'),
    alignItems: 'center',
    // or 'stretch'
  },
  SignIn:{
      width: wp('70'),
      height: hp('8'),
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth:1,
      borderColor:'blue',
      borderRadius:10,
      marginVertical:hp('6')
      
  },
});
