import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Image,
  TextInput,
  Pressable,
  Button,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function School_Code() {
  const [text, setText] = useState('');

   const onPressFunction=()=>{
       alert('clicked')
   }

  return (
    <View style={{flex: 1}}>
      <View>

        <ImageBackground
          source={require('../assests/login_background.png')}
          style={styles.backgroundImage}>
          <Image
            style={styles.image}
            source={require('../assests/login_logo.png')}
          />

          <TextInput
    textAlign={'center'}

            style={styles.TextInput}
            onChangeText={(text) => setText(text)}
            value={text}
          />
          <Pressable onPress={onPressFunction}>
     <Text>Dont Know School Code?</Text>
     </Pressable>

     <View style={styles.button}>
          <Button
            onPress={onPressFunction}
            title="Button Three"
            color="#FF3D00"
          />
        </View> 
        </ImageBackground>
      </View>
    </View>
  );
}

let styles = StyleSheet.create({
  backgroundImage: {
    resizeMode: 'cover',
    width: wp('100'),
    height: hp('100'),
    alignItems: 'center',
    // or 'stretch'
  },
  image: {
    width: wp('70'),
    height: hp('25'),
  },
  TextInput: {
    width: wp('88'),
    height: hp('8'),
    borderWidth: 1,
    borderColor: '#000',
    fontSize:22,
  },
  button:{
      width: wp('85'),
      height: hp('8'),

  }
});
