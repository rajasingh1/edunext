import React from "react";

import Screen1 from '../Screens/Screen1'
import Screen2 from '../Screens/Screen2'
import Index from '../Screens/DashBoard/Index'
import { createDrawerNavigator } from "@react-navigation/drawer";


const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Dashboard" component={Index} />
      <Drawer.Screen name="Screen1" component={Screen1} />
      <Drawer.Screen name="Screen2" component={Screen2} />
    </Drawer.Navigator>
  );
}

export default DrawerNavigator;