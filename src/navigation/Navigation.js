
import 'react-native-gesture-handler';


import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import School_Code from '../Screens/School_Code'
import Login from '../Screens/Login'
import Index from '../Screens/DashBoard/Index'
import DrawerNavigator from './DrawerNavigator'

const Stack = createStackNavigator();


export class Navigation extends Component {
    render() {
        return (
            <NavigationContainer>
            <Stack.Navigator
             screenOptions={{
              headerShown: false,
            }}
            >
                 <Stack.Screen
             
             name="Drawer"
             component={DrawerNavigator}
           />
                <Stack.Screen
                 
                 name="Dashboard"
                 component={School_Code}
               />
              <Stack.Screen
             
                name="SchoolCode"
                component={Index}
              />
               <Stack.Screen
                
                name="Login"
                component={Login}
              />
              
            
            </Stack.Navigator>
            
          </NavigationContainer>
        )
    }
}

export default Navigation

