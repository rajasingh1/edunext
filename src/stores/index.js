import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import AsyncStorage from '@react-native-community/async-storage';


import appReducer from './reducers'



const persistConfig = {
    key: 'root',
  
    storage: AsyncStorage,
    stateReconciler: autoMergeLevel2,
    whitelist: ['login']
  };

const persistedReducer = persistReducer(persistConfig, appReducer);
// const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
    persistedReducer,
    undefined,
  applyMiddleware(thunk),
  );

  export const persistor = persistStore(store);

  // TODO: remove `persistor.purge()` to persist your application data
// persistor.purge();