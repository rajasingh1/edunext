package com.edunext;
import android.os.Bundle;

import org.devio.rn.splashscreen.SplashScreen; // here 


import com.facebook.react.ReactActivity;



public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "Edunext";
  }

@Override
  protected void onCreate(Bundle savedInstanceState) {
    SplashScreen.show(this);
    // SplashScreen.show(this, R.style.SplashStatusBarTheme);

    super.onCreate(savedInstanceState);
    // RNBootSplash.init(R.drawable.bootsplash, MainActivity.this); // <- display the generated bootsplash.xml drawable over our MainActivity
  }
}

